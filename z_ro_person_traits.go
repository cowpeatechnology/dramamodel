package model

// PersonTrait 族人特质
type PersonTrait struct {
	Id       int32   `json:"id"`
	Name     string  `json:"name" validate:"required"`
	Desc     string  `json:"desc"`
	Type     int32   `json:"type" validate:"required"`
	SubType  int32   `json:"sub_type"`
	Quality  int32   `json:"quality" validate:"min=0"`
	Ratio    bool    `json:"ratio"`
	Property int32   `json:"property"`
	Prob     int32   `json:"prob"`
	Value    float32 `json:"value" validate:"required"`
}

type GetsPersonTraitReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}

type GetsPersonTraitRes struct {
	Page         int32          `json:"page"`
	PerPage      int32          `json:"per_page"`
	Total        int32          `json:"total"`
	PersonTraits []*PersonTrait `json:"person_traits"`
}

type CreatePersonTraitReq struct {
	*PersonTrait
}

type CreatePersonTraitRes struct {
	*PersonTrait
}

type UpdatePersonTraitReq struct {
	*PersonTrait
}
