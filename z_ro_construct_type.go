package model

// ConstructType 建筑主类型
type ConstructType struct {
	ConstructTypeId int32  `json:"construct_type_id" validate:"required,gte=0"` // 建筑主类型编号
	Name            string `json:"name" validate:"required"`                    // 名称
	Desc            string `json:"desc" validate:"required"`                    // 描述s
}

type GetsConstructTypeReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}

type GetsConstructTypeRes struct {
	Page           int32            `json:"page"`
	PerPage        int32            `json:"per_page"`
	Total          int32            `json:"total"`
	ConstructTypes []*ConstructType `json:"construct_types"`
}

type CreateConstructTypeReq struct {
	*ConstructType
}

type CreateConstructTypeRes struct {
	*ConstructType
}

type UpdateConstructTypeReq struct {
	*ConstructType
}

type UpdateConstructTypeRes struct {
	*ConstructType
}
