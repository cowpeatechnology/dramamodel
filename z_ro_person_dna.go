package model

// PersonDna 族人传承序列
type PersonDna struct {
	Id     int32  `json:"id" validate:"required"`
	RaceId int32  `json:"race_id" validate:"required"`
	Name   string `json:"name" validate:"required"`
	Count  int32  `json:"count" validate:"required"`
}

type GetsPersonDnaReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}

type GetsPersonDnaRes struct {
	Page       int32        `json:"page"`
	PerPage    int32        `json:"per_page"`
	Total      int32        `json:"total"`
	PersonDnas []*PersonDna `json:"person_dnas"`
}

type CreatePersonDnaReq struct {
	*PersonDna
}

type CreatePersonDnaRes struct {
	*PersonDna
}

type UpdatePersonDnaReq struct {
	*PersonDna
}
