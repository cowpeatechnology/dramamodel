package model

type FamilyHouseInfo struct {
	HouseInfoID     int32   `json:"house_info_id"`    // 家园建筑信息编号
	Type            int32   `json:"type"`             // 建筑类型
	HousePrice      string  `json:"house_price"`      // 建筑价格 (通过公式计算)
	LengthTime      string  `json:"length_time"`      // 建筑时间 (通过公式计算)
	Name            string  `json:"name"`             // 建筑名称
	Desc            string  `json:"desc"`             // 建筑描述
	Picture         string  `json:"picture"`          // 图标
	ImpactAttribute []int32 `json:"impact_attribute"` // 入驻族人属性影响
}

// TODO 建筑的等级有误填写有误
// 生产资源建筑不需要增加容量，本身我们自己的资源是以物品的形式存在的。
// 建筑的解锁条件 （他是根据掌门等级和休闲等级（也就是任务完成度）来限制解锁。我们可以根据定制玩家角色的等级，或者主城（通天殿）的等级来限制建筑的解锁）

type GetsFamilyHouseInfoReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}

type GetsFamilyHouseInfoRes struct {
	Page             int32              `json:"page"`
	PerPage          int32              `json:"per_page"`
	Total            int32              `json:"total"`
	FamilyHouseInfos []*FamilyHouseInfo `json:"family_house_infos"`
}

type CreateFamilyHouseInfoReq struct {
	*FamilyHouseInfo
}

type CreateFamilyHouseInfoRes struct {
	*FamilyHouseInfo
}

type UpdateFamilyHouseInfoReq struct {
	*FamilyHouseInfo
}

type UpdateFamilyHouseInfoRes struct {
	*FamilyHouseInfo
}
