package model

// MapPos 据点
type MapPos struct {
	MapPosId int32  `json:"map_pos_id" validate:"required,gte=0"` // 位置编号
	Landform int32  `json:"landform" validate:"required"`         // 地形（军事加成等）
	Resource int32  `json:"resource" validate:"required"`         // 资源（采集加成等）
	PosX     int32  `json:"pos_x" validate:"required"`            // 坐标 X 零左
	PosY     int32  `json:"pos_y" validate:"required"`            // 坐标 Y 零上
	Name     string `json:"name"`                                 //据点名称
	Picture  string `json:"picture"`                              // 图标
}

type GetsMapPosReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}

type GetsMapPosRes struct {
	Page     int32     `json:"page"`
	PerPage  int32     `json:"per_page"`
	Total    int32     `json:"total"`
	MapPoses []*MapPos `json:"map_poses"`
}

type CreateMapPosReq struct {
	*MapPos
}

type CreateMapPosRes struct {
	*MapPos
}

type UpdateMapPosReq struct {
	*MapPos
}

type UpdateMapPosRes struct {
	*MapPos
}
