package model

// DeviceWord 装备词条
type DeviceWord struct {
	Id       int32   `json:"id"`
	Name     string  `json:"name" validate:"required"`
	Desc     string  `json:"desc"`
	Type     int32   `json:"type" validate:"required"`
	SubType  int32   `json:"sub_type"`
	Quality  int32   `json:"quality" validate:"required"`
	Ratio    bool    `json:"ratio"`
	Property int32   `json:"property"`
	Prob     int32   `json:"prob"`
	Value    float32 `json:"value" validate:"required"`
}

type GetsDeviceWordReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}

type GetsDeviceWordRes struct {
	Page        int32         `json:"page"`
	PerPage     int32         `json:"per_page"`
	Total       int32         `json:"total"`
	DeviceWords []*DeviceWord `json:"device_words"`
}

type CreateDeviceWordReq struct {
	*DeviceWord
}

type CreateDeviceWordRes struct {
	*DeviceWord
}

type UpdateDeviceWordReq struct {
	*DeviceWord
}

type UpdateDeviceWordRes struct {
	*DeviceWord
}
