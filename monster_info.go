package model

type GetsMonsterInfoReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}

type GetsMonsterInfoRes struct {
	Page         int32          `json:"page"`
	PerPage      int32          `json:"per_page"`
	Total        int32          `json:"total"`
	MonsterInfos []*MonsterInfo `json:"monster_infos"`
}

type CreateMonsterInfoReq struct {
	*MonsterInfo
}

type CreateMonsterInfoRes struct {
	*MonsterInfo
}

type UpdateMonsterInfoReq struct {
	*MonsterInfo
}

type UpdateMonsterInfoRes struct {
	*MonsterInfo
}

type MonsterRewardProb struct {
	PointWeights int32 `json:"point_weights"` // 品质（这里的品质和族人品质有区别）
	Pos          int32 `json:"pos"`           // 格子
	PosProb      int32 `json:"pos_prob"`      // 概率
}

type GetsMonsterRewardProbReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}

type GetsMonsterRewardProbRes struct {
	MonsterRewardProb []*MonsterRewardProb `json:"monster_reward_prob"`
}

type CreateMonsterRewardProbReq struct {
	*MonsterRewardProb
}

type CreateMonsterRewardProbRes struct {
	*MonsterRewardProb
}

// MonsterInfo 野怪基础信息表
type MonsterInfo struct {
	MonsterInfoId         int32   `json:"monster_info_id"`         // 排序编号
	Name                  string  `json:"name"`                    // 怪物名称
	Level                 int32   `json:"level"`                   // 怪物等级
	Quality               int32   `json:"quality"`                 // 品质
	SkillId               []int32 `json:"skillId"`                 // 技能ID
	AdditiveAttributeType int32   `json:"additive_attribute_type"` // 加成属性类型
	AttributeValue        float64 `json:"attribute_value"`         // 属性值
	Picture               string  `json:"picture"`                 // 图标
}
