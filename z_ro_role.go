package model

// Role 职位
type Role struct {
	RoleId int32   `json:"role_id" validate:"required,gte=0"` // 职位编号
	Name   string  `json:"name" validate:"required"`          // 职位名
	Jobs   []int32 `json:"jobs" validate:"required"`          // 职能集
}

//job ：解散权，修改王国信息权，外交权，人员管理权，部队权限，建筑权，贸易权，道路设置权

type GetsRoleReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}

type GetsRoleRes struct {
	Page    int32   `json:"page"`
	PerPage int32   `json:"per_page"`
	Total   int32   `json:"total"`
	Roles   []*Role `json:"roles"`
}

type CreateRoleReq struct {
	*Role
}

type CreateRoleRes struct {
	*Role
}

type UpdateRoleReq struct {
	*Role
}

type UpdateRoleRes struct {
	*Role
}
