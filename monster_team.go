package model

// MonsterTeam 野怪队伍
type MonsterTeam struct {
	MonsterTeamID   int32       `json:"monster_team_id"`
	Monsters        []*Monsters `json:"monsters"`          //野怪集合
	ArmyAttributeID int32       `json:"army_attribute_id"` //兵种属性编号
}

type Monsters struct {
	MonsterInfoId int32 `json:"monster_info_id"` //野怪信息ID
	SoldiersCount int32 `json:"soldiers_count"`  //士兵数
}

type GetsMonsterTeamReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}

type GetsMonsterTeamRes struct {
	Page         int32          `json:"page"`
	PerPage      int32          `json:"per_page"`
	Total        int32          `json:"total"`
	MonsterTeams []*MonsterTeam `json:"monster_teams"`
}

type CreateMonsterTeamReq struct {
	*MonsterTeam
}

type CreateMonsterTeamRes struct {
	*MonsterTeam
}

type UpdateMonsterTeamReq struct {
	*MonsterTeam
}

type UpdateMonsterTeamRes struct {
	*MonsterTeam
}
