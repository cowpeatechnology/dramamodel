package model

// MapPathStatus 初始化的道路状态
type MapPathStatus struct {
	MapPathId int32 `json:"map_path_id" validate:"required,gte=0"` // 道路状态编号
	Level     int32 `json:"level" validate:"required"`             // 道路等级
}

type GetsMapPathStatusReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}

type GetsMapPathStatusRes struct {
	Page            int32            `json:"page"`
	PerPage         int32            `json:"per_page"`
	Total           int32            `json:"total"`
	MapPathStatuses []*MapPathStatus `json:"map_path_statuses"`
}

type CreateMapPathStatusReq struct {
	*MapPathStatus
}

type CreateMapPathStatusRes struct {
	*MapPathStatus
}

type UpdateMapPathStatusReq struct {
	*MapPathStatus
}

type UpdateMapPathStatusRes struct {
	*MapPathStatus
}
