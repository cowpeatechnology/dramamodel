package model

// TroopConfigurationTable 兵种配置表
type TroopConfigurationTable struct {
	Id                int32 `json:"id"`
	SoldierType       int32 `json:"soldier_type"`       //士兵类型
	SoldierLevel      int32 `json:"soldier_level"`      //士兵等级
	ProductionTime    int64 `json:"production_time"`    //生产时间
	MoneyType         int32 `json:"money_type"`         //货币类型
	MoneyCount        int32 `json:"money_count"`        //货币数量
	AssistantsSoldier int32 `json:"assistants_soldier"` //协助兵力
	RewardsType       int32 `json:"rewards_type"`       //奖励类型
	AssistantsRewards int32 `json:"assistants_rewards"` //协助者的奖励（这里对应的砖石）
}

type GetsTroopConfigurationTableReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}

type GetsTroopConfigurationTableRes struct {
	Page                     int32                      `json:"page"`
	PerPage                  int32                      `json:"per_page"`
	Total                    int32                      `json:"total"`
	TroopConfigurationTables []*TroopConfigurationTable `json:"troop_configuration_tables"`
}

type CreateTroopConfigurationTableReq struct {
	*TroopConfigurationTable
}

type CreateTroopConfigurationTableRes struct {
	*TroopConfigurationTable
}

type UpdateTroopConfigurationTableReq struct {
	*TroopConfigurationTable
}

type UpdateTroopConfigurationTableRes struct {
	*TroopConfigurationTable
}
