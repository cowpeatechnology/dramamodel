package model

// ConstructInfo 建筑信息
type ConstructInfo struct {
	ConstructInfoId int32  `json:"construct_info_id"` // 建筑信息编号
	ConstructTypeId int32  `json:"construct_type_id"` // 类型
	SubTypeId       int32  `json:"sub_type_id"`       //子类型ID（比如：兵营（骑，剑，弓，枪））
	RaceId          int32  `json:"race_id"`           // 种族
	Name            string `json:"name"`              // 名称
	Desc            string `json:"desc"`              // 描述

	Picture string `json:"picture"` // 图标

	BuildNeedGoods      []*BuildNeedGoods   `json:"build_need_goods"`      // 建造所需物品
	BuildNeedConstructs []*ConstructPremise `json:"build_need_constructs"` // 建造所需建筑及其等级
}
type BuildNeedGoods struct {
	Level     int32        `json:"level" validate:"required"` // 升级目标等级
	NeedTime  int64        `json:"need_time"`                 //建造升级的时间
	NeedGoods []*GoodCount `json:"need_goods"`
}

type ConstructPremise struct {
	Level          int32           `json:"level" validate:"required"` // 升级目标等级
	NeedConstructs []NeedConstruct `json:"need_constructs"`           //前置建筑
}
type NeedConstruct struct {
	ConstructInfoId int32 `json:"construct_info_id" validate:"required"`     // 所需建筑
	ConstructLevel  int32 `json:"construct_level" validate:"required,min=1"` // 所需建筑等级
}

/////////////////////////////// API请求 ////////////////////////////////

type GetsConstructInfoReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}

type GetsConstructInfoRes struct {
	Page           int32            `json:"page"`
	PerPage        int32            `json:"per_page"`
	Total          int32            `json:"total"`
	ConstructInfos []*ConstructInfo `json:"construct_infos"`
}

type CreateConstructInfoReq struct {
	*ConstructInfo
}

type CreateConstructInfoRes struct {
	*ConstructInfo
}

type UpdateConstructInfoReq struct {
	*ConstructInfo
}

type UpdateConstructInfoRes struct {
	*ConstructInfo
}

/////////////////////////////// 建筑衍生 ////////////////////////////////

// TODO

// ProduceConstructInfo 生产类建筑信息
type ProduceConstructInfo struct {
	ConstructInfoId int32        `json:"construct_info_id" validate:"required"` // 建筑信息编号
	ProduceCosts    []*GoodCount `json:"produce_costs" validate:"required"`     // 生产消耗
	ProduceResults  []*GoodCount `json:"produce_results" validate:"required"`   // 生产产出
}

type UpdateProduceConstructInfoReq struct {
	ProduceCosts   []*GoodCount `json:"produce_costs" validate:"required"`   // 生产消耗
	ProduceResults []*GoodCount `json:"produce_results" validate:"required"` // 生产产出
}

type UpdateProduceConstructInfoRes struct {
	ProduceCosts   []*GoodCount `json:"produce_costs" validate:"required"`   // 生产消耗
	ProduceResults []*GoodCount `json:"produce_results" validate:"required"` // 生产产出
}

// TransferConstructInfo 转换类建筑信息
type TransferConstructInfo struct {
	ConstructInfoId int32            `json:"construct_info_id" validate:"required"` // 建筑信息编号
	RecipePremises  []*RecipePremise `json:"recipe_premises" validate:"required"`   // 配方集
}

type UpdateTransferConstructInfoReq struct {
	RecipePremises []*RecipePremise `json:"recipe_premises" validate:"required"` // 配方集
}

type UpdateTransferConstructInfoRes struct {
	RecipePremises []*RecipePremise `json:"recipe_premises" validate:"required"` // 配方集
}

// EffectConstructInfo 影响类建筑信息
type EffectConstructInfo struct {
	ConstructInfoId int32              `json:"construct_info_id" validate:"required"` // 建筑信息编号
	Effects         []*AttributeEffect `json:"effects" validate:"required"`           // 影响集
}

type UpdateEffectConstructInfoReq struct {
	Effects []*AttributeEffect `json:"effects" validate:"required"` // 影响集
}

type UpdateEffectConstructInfoRes struct {
	Effects []*AttributeEffect `json:"effects" validate:"required"` // 影响集
}

// HomeConstructInfo 王国类建筑信息
type HomeConstructInfo struct {
	ConstructInfoId int32 `json:"construct_info_id" validate:"required"` // 建筑信息编号
}
