package model

type UserPlayerGuild struct {
	Id         int64  `json:"id" validate:"required,gte=0"`          // 编号
	Pos        int32  `json:"pos" validate:"required,gte=1"`         // 王国标记点
	MaxMembers int32  `json:"max_members" validate:"required,gte=1"` // 最大成员数
	Race       int32  `json:"race" validate:"required"`              // 王国种族
	PlayerName string `json:"player_name" validate:"required"`       // 玩家名称
	GuildName  string `json:"guild_name" validate:"required"`        // 王国名称
	Content    string `json:"content" validate:"required"`           // 王国公告
	Passport   string `json:"passport" validate:"required"`          // 用户账号
	Password   string `json:"password" validate:"required"`          // 用户密码
	//Level      int32  `json:"level"`                        // 王国等级 （暂时不用）
}

type GetsUserPlayerGuildReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}

type GetsUserPlayerGuildRes struct {
	Page             int32              `json:"page"`
	PerPage          int32              `json:"per_page"`
	Total            int32              `json:"total"`
	UserPlayerGuilds []*UserPlayerGuild `json:"user_player_guilds"`
}

type CreateUserPlayerGuildReq struct {
	*UserPlayerGuild
}

type CreateUserPlayerGuildRes struct {
	*UserPlayerGuild
}

type UpdateUserPlayerGuildReq struct {
	*UserPlayerGuild
}

type UpdateUserPlayerGuildRes struct {
	*UserPlayerGuild
}
