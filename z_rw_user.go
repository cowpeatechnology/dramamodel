package model

// User 初始化的玩家
type User struct {
	UserId     int64  `json:"user_id" validate:""`
	Passport   string `json:"passport"`    //用户账号
	Password   string `json:"password"`    //用户密码
	UserStatus string `json:"user_status"` //用户状态
}

type GetsUserReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}

type GetsUserRes struct {
	Page    int32   `json:"page"`
	PerPage int32   `json:"per_page"`
	Total   int32   `json:"total"`
	Users   []*User `json:"users"`
}

type CreateUserReq struct {
	*User
}

type CreateUserRes struct {
	*User
}

type UpdateUserReq struct {
	*User
}

type UpdateUserRes struct {
	*User
}
