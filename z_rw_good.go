package model

type Good struct {
	Id      int32  `json:"id" validate:"required"`   // 道具编号
	Type    int32  `json:"type" validate:"required"` // 道具类型
	SubType int32  `json:"sub_type"`                 // 子类型
	Recycle int32  `json:"recycle" validate:"min=0"` // 回收价格
	Target  int32  `json:"target"`                   // 使用对象
	Value   int32  `json:"value"`                    // 值
	Name    string `json:"name" validate:"required"` // 名称
	Desc    string `json:"desc" validate:"required"` // 描述

	Picture string `json:"picture"` // 图标
	Price   int32  `json:"price"`   // 价格
}

type GoodCount struct {
	GoodId int32 `json:"good_id" validate:"required"`
	Count  int32 `json:"count" validate:"min=1"`
}

type GoodCountName struct {
	*GoodCount `validate:"required,dive,required"`
	GoodsName  string `json:"goods_name" validate:"required"`
}

type GoodItems struct {
	Id      int32  `json:"id"`
	Name    string `json:"name"`
	Type    int32  `json:"type"`
	SubType int32  `json:"sub_type"`
}

type CreateGoodReq struct {
	*Good
}

type CreateGoodRes struct {
	*Good
}

type UpdateGoodReq struct {
	*Good
}

type UpdateGoodRes struct {
	*Good
}

type GetsGoodReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
	Type    int32 `form:"type"`
	SubType int32 `form:"sub_type"`
}

type GetsGoodRes struct {
	Page    int32   `json:"page"`
	PerPage int32   `json:"per_page"`
	Total   int32   `json:"total"`
	Goods   []*Good `json:"goods"`
}

// ForgedMaterialsInfo 物品扩展表
// ForgedMaterialsInfo 锻造材料，用于装备锻造时提高装备词条的概率
type ForgedMaterialsInfo struct {
	ID     int32   `json:"id"`
	GoodId int32   `json:"good_id"` //物品ID
	Name   string  `json:"name"`    // 物品名称
	Words  []int32 `json:"words"`   // 词条集合
	Prob   []int32 `json:"prob"`    // 出现齿条的概率
}

type GetsForgedMaterialsInfoReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}

type GetsForgedMaterialsInfoRes struct {
	Page                 int32                  `json:"page"`
	PerPage              int32                  `json:"per_page"`
	Total                int32                  `json:"total"`
	ForgedMaterialsInfos []*ForgedMaterialsInfo `json:"equipment_infos"`
}

type CreateForgedMaterialsInfoReq struct {
	*ForgedMaterialsInfo
}

type CreateForgedMaterialsInfoRes struct {
	*ForgedMaterialsInfo
}

type UpdateForgedMaterialsInfoReq struct {
	*ForgedMaterialsInfo
}

type UpdateForgedMaterialsInfoRes struct {
	*ForgedMaterialsInfo
}
