package model

// 世界任务信息

type WorldTasksInfo struct {
	ID         int32        `json:"id"`          // 编号
	TargetId   []int32      `json:"target_id"`   //任务目标ID
	Chapter    string       `json:"chapter"`     // 章节
	Name       string       `json:"name"`        //名称
	Desc       string       `json:"desc"`        // 描述
	Picture    string       `json:"picture"`     // 图片地址
	TaskReward []TaskReward `json:"task_reward"` // 奖励
}
type GetsWorldTasksInfoReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}

type GetsWorldTasksInfoRes struct {
	Page           int32             `json:"page"`
	PerPage        int32             `json:"per_page"`
	Total          int32             `json:"total"`
	WorldTasksInfo []*WorldTasksInfo `json:"world_tasks_info"`
}

type TaskReward struct {
	RewardId    int32 `json:"reward_id"`    // 奖励ID
	RewardType  int32 `json:"reward_type"`  // 奖励类型
	RewardCount int32 `json:"reward_count"` // 奖励数量
}
type CreateWorldTasksInfoReq struct {
	*WorldTasksInfo
}

type CreateWorldTasksInfoRes struct {
	*WorldTasksInfo
}

type UpdateWorldTasksInfoReq struct {
	*WorldTasksInfo
}

type UpdateWorldTasksInfoRes struct {
	*WorldTasksInfo
}

// 任务目标

type TaskTarget struct {
	TaskTargetId int32  `json:"task_target_id"` // 任务编号
	Number       int32  `json:"number"`         // 数量
	Desc         string `json:"desc"`           // 描述
	Icon         string `json:"icon"`           // 图标
}

type GetsTaskTargetReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}

type GetsTaskTargetRes struct {
	Page       int32         `json:"page"`
	PerPage    int32         `json:"per_page"`
	Total      int32         `json:"total"`
	TaskTarget []*TaskTarget `json:"task_target"`
}

type CreateTaskTargetReq struct {
	*TaskTarget
}

type CreateTaskTargetRes struct {
	*TaskTarget
}

type UpdateTaskTargetReq struct {
	*TaskTarget
}

type UpdateTaskTargetRes struct {
	*TaskTarget
}
