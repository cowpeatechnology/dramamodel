package model

type TaskAssist struct {
	TaskAssistId   int32          `json:"task_assist_id"`   // 协助任务id
	AttributeId    int32          `json:"attribute_id"`     // 属性Id
	Name           string         `json:"name"`             // 任务名称
	PersonNeedTime int64          `json:"person_need_time"` // 族人需求时间
	LevelReward    []*LevelReward `json:"level_reward"`     // 属性奖励(一级，二级，三级)
	Dec            string         `json:"dec"`              // 简介
	TaskNeedTime   int64          `json:"task_need_time"`   // 任务需要时间
}

type LevelReward struct {
	RewardId       int32   `json:"reward_id"`       // 奖励ID
	RewardType     int32   `json:"reward_type"`     // 奖励类型（物品，装备）
	Count          int32   `json:"count"`           // 奖励数量
	AttributeValue float32 `json:"attribute_value"` //属性最大值
}

type GetsTaskAssistReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}

type GetsTaskAssistRes struct {
	Page       int32         `json:"page"`
	PerPage    int32         `json:"per_page"`
	Total      int32         `json:"total"`
	TaskAssist []*TaskAssist `json:"task_assist"`
}

type CreateTaskAssistReq struct {
	*TaskAssist
}

type CreateTaskAssistRes struct {
	*TaskAssist
}

type UpdateTaskAssistReq struct {
	*TaskAssist
}

type UpdateTaskAssistRes struct {
	*TaskAssist
}
