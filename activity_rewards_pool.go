package model

// ActivityRewardsPool 活动奖励池
type ActivityRewardsPool struct {
	Id          int32 `json:"id"`
	RewardId    int32 `json:"reward_id"`
	RewardType  int32 `json:"reward_type"`
	RewardCount int32 `json:"reward_count"`
}
type GetsActivityRewardsPoolReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}
type GetsActivityRewardsPoolRes struct {
	Page                int32                  `json:"page"`
	PerPage             int32                  `json:"per_page"`
	Total               int32                  `json:"total"`
	ActivityRewardsPool []*ActivityRewardsPool `json:"activity_rewards_pool"`
}

type CreateActivityRewardsPoolReq struct {
	*ActivityRewardsPool
}

type CreateActivityRewardsPoolRes struct {
	*ActivityRewardsPool
}

type UpdateActivityRewardsPoolReq struct {
	*ActivityRewardsPool
}

type UpdateActivityRewardsPoolRes struct {
	*ActivityRewardsPool
}

// 签到奖励
type SignInRewards struct {
	SignInRewardsID int32 `json:"sign_in_rewards_id"`
	DayId           int32 `json:"day_id"`          // 天数
	RewardsPoolId   int32 `json:"rewards_pool_id"` // 奖励池ID
}
type GetsSignInRewardsRes struct {
	SignInRewards []*SignInRewards `json:"sign_in_rewards"`
}

type CreateSignInRewardsReq struct {
	*SignInRewards
}

type CreateSignInRewardsRes struct {
	*SignInRewards
}

// 宝箱
type TreasureChest struct {
	TreasureChestID int32   `json:"treasure_chest_id"` // 宝箱ID
	Name            string  `json:"name"`              // 宝箱名称
	ActivityType    int32   `json:"activity_type"`     // 活动类型
	ReceiveRange    []int32 `json:"receive_range"`     // 领取范围
	RewardsPoolId   []int32 `json:"rewards_pool_id"`   // 奖励池ID
	Icon            string  `json:"icon"`              // 图标
}

type GetsTreasureChestReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}
type GetsTreasureChestRes struct {
	Page          int32            `json:"page"`
	PerPage       int32            `json:"per_page"`
	Total         int32            `json:"total"`
	TreasureChest []*TreasureChest `json:"treasure_chest"`
}

type CreateTreasureChestReq struct {
	*TreasureChest
}

type CreateTreasureChestRes struct {
	*TreasureChest
}

type UpdateTreasureChestReq struct {
	*TreasureChest
}

type UpdateTreasureChestRes struct {
	*TreasureChest
}
