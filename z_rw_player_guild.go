package model

// PlayerGuild 初始化的王国-角色对应关系
type PlayerGuild struct {
	PlayerId   int64 `json:"player_id" validate:"required,gt=0"` // 用户编号
	GuildId    int32 `json:"guild_id" validate:"required,gt=0"`  // 王国编号
	Authority  int32 `json:"authority" validate:"required"`      // 职位
	Contribute int32 `json:"contribute"`                         // 贡献
}

type GetsPlayerGuildReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}

type GetsPlayerGuildRes struct {
	Page         int32          `json:"page"`
	PerPage      int32          `json:"per_page"`
	Total        int32          `json:"total"`
	PlayerGuilds []*PlayerGuild `json:"player_guilds"`
}

type CreatePlayerGuildReq struct {
	*PlayerGuild
}

type CreatePlayerGuildRes struct {
	*PlayerGuild
}

type UpdatePlayerGuildReq struct {
	*PlayerGuild
}

type UpdatePlayerGuildRes struct {
	*PlayerGuild
}
