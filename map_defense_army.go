package model

// 大地图守军表
type MapDefenseArmy struct {
	Id            int32     `json:"id"`              // 守军表编号
	MapPosId      int32     `json:"map_pos_id"`      // 据点ID
	IsMapPos      bool      `json:"is_map_pos"`      // true:据点 false:野怪点
	MonsterTeamID []int32   `json:"monster_team_id"` // 能产生野怪队伍ID
	DefenseCount  int32     `json:"defense_count"`   // 守军军队数量
	RecoverCount  int32     `json:"recover_count"`   // 恢复数量（据点占领后的系统守军）
	Reward        []*Reward `json:"reward"`          // 奖励（守军首次奖励）（野怪点奖励）
	PointWeights  int32     `json:"point_weights"`   // 据点权重
}

type GetsMapDefenseArmyReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}

type GetsMapDefenseArmyRes struct {
	Page             int32             `json:"page"`
	PerPage          int32             `json:"per_page"`
	Total            int32             `json:"total"`
	MapDefenseArmies []*MapDefenseArmy `json:"map_defense_armies"`
}

type CreateMapDefenseArmyReq struct {
	*MapDefenseArmy
}

type CreateMapDefenseArmyRes struct {
	*MapDefenseArmy
}

type UpdateMapDefenseArmyReq struct {
	*MapDefenseArmy
}

type UpdateMapDefenseArmyRes struct {
	*MapDefenseArmy
}
