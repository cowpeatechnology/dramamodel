package model

// ArmyType 兵种
type ArmyType struct {
	ArmTypeId int32  `json:"arm_type_id" validate:"required,gte=0"` // 兵种类型编号
	Name      string `json:"name"  validate:"required"`             // 名称
}

type GetsArmyTypeReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}

type GetsArmyTypeRes struct {
	Page      int32       `json:"page"`
	PerPage   int32       `json:"per_page"`
	Total     int32       `json:"total"`
	ArmyTypes []*ArmyType `json:"army_types"`
}

type CreateArmyTypeReq struct {
	*ArmyType
}

type CreateArmyTypeRes struct {
	*ArmyType
}

type UpdateArmyTypeReq struct {
	*ArmyType
}

type UpdateArmyTypeRes struct {
	*ArmyType
}
