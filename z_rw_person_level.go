package model

type PersonLevel struct {
	Level      int32 `json:"level" validate:"required"`    // 族人等级
	Race       int32 `json:"race" validate:"required"`     // 种族
	Quality    int32 `json:"quality" validate:"required"`  // 族人品质
	UpgradeExp int32 `json:"upgrade_exp" validate:"min=0"` // 升级经验
	Point      int32 `json:"point" validate:"min=0"`       // 族人小等级
}

type CreatePersonLevelReq struct {
	*PersonLevel
}

type CreatePersonLevelRes struct {
	*PersonLevel
}

type UpdatePersonLevelReq struct {
	*PersonLevel
}

type UpdatePersonLevelRes struct {
	*PersonLevel
}

type GetsPersonLevelReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
	Race    int32 `form:"race"`
	Quality int32 `form:"quality"`
}

type GetsPersonLevelRes struct {
	Page         int32          `json:"page"`
	PerPage      int32          `json:"per_page"`
	Race         int32          `json:"race"`
	Quality      int32          `json:"quality"`
	Total        int32          `json:"total"`
	PersonLevels []*PersonLevel `json:"person_levels"`
}
