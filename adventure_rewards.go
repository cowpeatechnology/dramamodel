package model

// AdventureRewards 奖励池
type AdventureRewards struct {
	ID         int32 `json:"id" gorm:"primary_key"`
	RewardType int32 `json:"reward_type"` // 奖励类型）// (1:武器奖励，2:装备奖励，3:适性书奖励，4:原材料奖励，5:经验书奖励)
	Level      int32 `json:"level"`       // 奖励池等级
	RewardId   int32 `json:"reward_id"`   // 奖励ID （对应物品、装备ID）
}

type GetsAdventureRewardsReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}

type GetsAdventureRewardsRes struct {
	Page             int32               `json:"page"`
	PerPage          int32               `json:"per_page"`
	Total            int32               `json:"total"`
	AdventureRewards []*AdventureRewards `json:"adventure_rewards"`
}

type CreateAdventureRewardsReq struct {
	*AdventureRewards
}

type CreateAdventureRewardsRes struct {
	*AdventureRewards
}

type UpdateAdventureRewardsReq struct {
	*AdventureRewards
}

type UpdateAdventureRewardsRes struct {
	*AdventureRewards
}

///////////////////////////////////////////////////

// RewardsTypeProb 奖励类型的刷新概率
type RewardsTypeProb struct {
	RewardType      int32   `json:"reward_type"`       //奖励类型
	RewardsTypeProb int32   `json:"rewards_type_prob"` //奖励类型概率
	RewardPos       []int32 `json:"reward_pos"`        // 奖励位置集
}

type GetsRewardsTypeProbReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}

type GetsRewardsTypeProbRes struct {
	Page              int32              `json:"page"`
	PerPage           int32              `json:"per_page"`
	Total             int32              `json:"total"`
	RewardsTypeProbes []*RewardsTypeProb `json:"rewards_type_probes"`
}

type CreateRewardsTypeProbReq struct {
	*RewardsTypeProb
}

type CreateRewardsTypeProbRes struct {
	*RewardsTypeProb
}

type UpdateRewardsTypeProbReq struct {
	*RewardsTypeProb
}

type UpdateRewardsTypeProbRes struct {
	*RewardsTypeProb
}

//////////////////////////////////////////////////

// RewardsProb 奖励的刷新概率
type RewardsProb struct {
	Level     int32 `json:"level"`      //奖励池
	RewardPos int32 `json:"reward_pos"` //奖励位置
	PosProb   int32 `json:"pos_prob"`   //奖励的位置概率
}
type GetsRewardsProbReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}

type GetsRewardsProbRes struct {
	Page          int32          `json:"page"`
	PerPage       int32          `json:"per_page"`
	Total         int32          `json:"total"`
	RewardsProbes []*RewardsProb `json:"rewards_probes"`
}

type CreateRewardsProbReq struct {
	*RewardsProb
}

type CreateRewardsProbRes struct {
	*RewardsProb
}

type UpdateRewardsProbReq struct {
	*RewardsProb
}

type UpdateRewardsProbRes struct {
	*RewardsProb
}
