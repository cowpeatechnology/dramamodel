package model

type GoodType struct {
	GoodTypeId int32  `json:"good_type_id" validate:"required,gte=0"` // 道具编号
	Name       string `json:"name"  validate:"required"`              // 名称
}

type GetsGoodTypeReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}

type GetsGoodTypeRes struct {
	Page      int32       `json:"page"`
	PerPage   int32       `json:"per_page"`
	Total     int32       `json:"total"`
	GoodTypes []*GoodType `json:"good_types"`
}

type CreateGoodTypeReq struct {
	*GoodType
}

type CreateGoodTypeRes struct {
	*GoodType
}

type UpdateGoodTypeReq struct {
	*GoodType
}

type UpdateGoodTypeRes struct {
	*GoodType
}
