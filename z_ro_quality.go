package model

type Quality struct {
	Id           int32  `json:"id" validate:"required,gte=0"`      // 品质Id
	DeviceWords  int32  `json:"device_words" validate:"required"`  // 装备最大词条数(最小0)
	PersonTraits int32  `json:"person_traits" validate:"required"` // 族人好特质数量(最小0)
	PersonProb   int32  `json:"person_prob" validate:"required"`   // 族人繁衍概率
	Name         string `json:"name" validate:"required"`          // 品质名称
}

type GetsQualityReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}

type GetsQualityRes struct {
	Page      int32      `json:"page"`
	PerPage   int32      `json:"per_page"`
	Total     int32      `json:"total"`
	Qualities []*Quality `json:"qualities"`
}

type CreateQualityReq struct {
	*Quality
}

type CreateQualityRes struct {
	*Quality
}

type UpdateQualityReq struct {
	*Quality
}

type UpdateQualityRes struct {
	*Quality
}

// QualityProb 品质合成概率
type QualityProb struct {
	ID          int32 `json:"id"`
	QualityId   int32 `json:"quality_id"`    // 品质ID
	UpQualityId int32 `json:"up_quality_id"` // 升级后的品质
	Prob        int32 `json:"prob"`          // 升级的概率
}

type GetsQualityProbReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}

type GetsQualityProbRes struct {
	Page          int32          `json:"page"`
	PerPage       int32          `json:"per_page"`
	Total         int32          `json:"total"`
	QualityProbes []*QualityProb `json:"quality_probes"`
}

type CreateQualityProbReq struct {
	*QualityProb
}

type CreateQualityProbRes struct {
	*QualityProb
}

type UpdateQualityProbReq struct {
	*QualityProb
}

type UpdateQualityProbRes struct {
	*QualityProb
}
