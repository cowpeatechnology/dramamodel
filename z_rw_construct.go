package model

// Construct 初始化的建筑
type Construct struct {
	Pos             int32 `json:"pos" validate:"required"`               // 建筑编号（位置编号）
	ConstructInfoId int32 `json:"construct_info_id" validate:"required"` // 建筑信息编号
	Level           int32 `json:"level" validate:"required"`             // 建筑等级 1-5
	Belong          int64 `json:"belong" validate:"required"`            // 归属王国
	Durability      int32 `json:"durability" validate:"required"`        // 耐久、剩余征服值，敌军占领时减少（0值时完成征服，变为空地），友军占领时增加（暂时统一最高100）
}

type GetsConstructReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}

type GetsConstructRes struct {
	Page       int32        `json:"page"`
	PerPage    int32        `json:"per_page"`
	Total      int32        `json:"total"`
	Constructs []*Construct `json:"constructs"`
}

type CreateConstructReq struct {
	*Construct
}

type CreateConstructRes struct {
	*Construct
}

type UpdateConstructReq struct {
	*Construct
}

type UpdateConstructRes struct {
	*Construct
}
