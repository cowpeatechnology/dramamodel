package model

import (
	"gitee.com/cowpeatechnology/pbc"
	pb "google.golang.org/protobuf/proto"
)

type Npc struct {
	Id   int64  `json:"id" validate:"required"`
	Name string `json:"name" validate:"required"`
	Race int32  `json:"race" validate:"required"`
	Dna  string `json:"dna" validate:"required"`
}

type CreateNpcReq struct {
	*Npc
}

type CreateNpcRes struct {
	*Npc
}

type UpdateNpcReq struct {
	*Npc
}

type UpdateNpcRes struct {
	*Npc
}

type NpcItems struct {
	Id   int32  `json:"id"`
	Name string `json:"name"`
}

type GetsNpcReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}

type GetsNpcRes struct {
	Page    int32  `json:"page"`
	PerPage int32  `json:"per_page"`
	Total   int32  `json:"total"`
	Npc     []*Npc `json:"npc"`
}

type CreateNpcLevelReq struct {
	*NpcLevelForm
}

type CreateNpcLevelRes struct {
	*NpcLevelForm
}

type UpdateNpcLevelReq struct {
	*NpcLevelForm
}

type UpdateNpcLevelRes struct {
	*NpcLevelForm
}

type GetsNpcLevelReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
	NpcId   int32 `json:"npc_id"`
}

type GetsNpcLevelRes struct {
	Page    int32           `json:"page"`
	PerPage int32           `json:"per_page"`
	Total   int32           `json:"total"`
	Level   []*NpcLevelForm `json:"level"`
}

type NpcLevelForm struct {
	NpcId int64 `json:"npc_id"`
	Level int32 `json:"level"`
	*pbc.NpcLevelData
}

func (nl *NpcLevelForm) ToBlob() (blob []byte, err error) {
	blob, err = pb.Marshal(nl.NpcLevelData)
	if err != nil {
		return
	}
	return
}

type NpcLevel struct {
	NpcId int64
	Level int32
	Data  []byte
}

type GetNpcCombatReq struct {
	NpcId    int32 `json:"npc_id" validate:"required"`
	NpcLevel int32 `json:"npc_level" validate:"required"`
}

type GetNpcCombatRes struct {
	NpcId    int32 `json:"npc_id"`
	NpcLevel int32 `json:"npc_level"`
	Combat   int32 `json:"combat"`
}
