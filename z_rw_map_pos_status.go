package model

// MapPosStatus 初始化的据点状态
type MapPosStatus struct {
	MapPosId int32 `json:"map_pos_id" validate:"required,gte=0"` // 位置编号
	Occupier int32 `json:"occupier" validate:"required"`         // 占有者
}
type GetsMapPosStatusReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}

type GetsMapPosStatusRes struct {
	Page           int32           `json:"page"`
	PerPage        int32           `json:"per_page"`
	Total          int32           `json:"total"`
	MapPosStatuses []*MapPosStatus `json:"map_pos_statuses"`
}

type CreateMapPosStatusReq struct {
	*MapPosStatus
}

type CreateMapPosStatusRes struct {
	*MapPosStatus
}

type UpdateMapPosStatusReq struct {
	*MapPosStatus
}

type UpdateMapPosStatusRes struct {
	*MapPosStatus
}
