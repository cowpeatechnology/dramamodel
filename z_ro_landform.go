package model

// Landform 地形
type Landform struct {
	LandformId int32              `json:"landform_id" validate:"required,gte=0"` // 地形编号
	Name       string             `json:"name"  validate:"required"`             // 名称
	Effects    []*AttributeEffect `json:"effects"  validate:"required"`          // 属性
}

type GetsLandformReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}

type GetsLandformRes struct {
	Page      int32       `json:"page"`
	PerPage   int32       `json:"per_page"`
	Total     int32       `json:"total"`
	Landforms []*Landform `json:"landforms"`
}

type CreateLandformReq struct {
	*Landform
}

type CreateLandformRes struct {
	*Landform
}

type UpdateLandformReq struct {
	*Landform
}

type UpdateLandformRes struct {
	*Landform
}
