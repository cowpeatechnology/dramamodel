package model

// Guild 初始化的王国
type Guild struct {
	Id         int32  `json:"id"  validate:"required,gte=0"` // 王国编号
	Icon       int32  `json:"icon" validate:"required"`      // 王国图片
	Level      int32  `json:"level"`                         // 王国等级 （暂时不用）
	MaxMembers int32  `json:"max_members"`                   // 最大成员数
	Race       int32  `json:"race" validate:"required"`      // 王国种族
	Name       string `json:"name" validate:"required"`      // 王国名称
	Content    string `json:"content"  validate:"required"`  // 王国公告
}

type GetsGuildReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}

type GetsGuildRes struct {
	Page    int32    `json:"page"`
	PerPage int32    `json:"per_page"`
	Total   int32    `json:"total"`
	Guilds  []*Guild `json:"guilds"`
}

type CreateGuildReq struct {
	*Guild
}

type CreateGuildRes struct {
	*Guild
}

type UpdateGuildReq struct {
	*Guild
}

type UpdateGuildRes struct {
	*Guild
}
