package model

type TaskGuildReward struct {
	Id           int32     `json:"id"`
	TaskType     int32     `json:"task_type"`
	Rewards      []*Reward `json:"reward"`
	TaskNeedTime int64     `json:"task_need_time"`
}

type Reward struct {
	RewardId   int32 `json:"reward_id"`   // 奖励ID
	RewardType int32 `json:"reward_type"` // 奖励类型（物品，装备）
	Count      int32 `json:"count"`       // 奖励数量
}

type GetsTaskGuildRewardReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}

type GetsTaskGuildRewardRes struct {
	Page             int32              `json:"page"`
	PerPage          int32              `json:"per_page"`
	Total            int32              `json:"total"`
	TaskGuildRewards []*TaskGuildReward `json:"task_guild_reward"`
}

type CreateTaskGuildRewardReq struct {
	*TaskGuildReward
}

type CreateTaskGuildRewardRes struct {
	*TaskGuildReward
}

type UpdateTaskGuildRewardReq struct {
	*TaskGuildReward
}

type UpdateTaskGuildRewardRes struct {
	*TaskGuildReward
}
