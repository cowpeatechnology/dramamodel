package model

type ConstructSubType struct {
	Id                 int32  `json:"id"`                                          // 编号
	ConstructTypeId    int32  `json:"construct_type_id" validate:"required,gte=0"` // 建筑主类型编号
	ConstructSubTypeId int32  `json:"construct_sub_type_id"`                       //建筑子类型编号
	Name               string `json:"name"`                                        //子类型名称
	Desc               string `json:"desc" validate:"required"`                    // 描述s
}

type GetsConstructSubTypeRes struct {
	Total             int32               `json:"total"`
	ConstructSubTypes []*ConstructSubType `json:"construct_sub_types"`
}

type CreateConstructSubTypeReq struct {
	*ConstructSubType
}

type CreateConstructSubTypeRes struct {
	*ConstructSubType
}

type UpdateConstructSubTypeReq struct {
	*ConstructSubType
}

type UpdateConstructSubTypeRes struct {
	*ConstructSubType
}
