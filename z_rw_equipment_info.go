package model

// EquipmentInfo 装备信息
type EquipmentInfo struct {
	EquipmentInfoId     int32                  `json:"equipment_info_id"`     // 装备编号
	EquipmentPosId      int32                  `json:"equipment_pos_id"`      // 装备位置
	Type                string                 `json:"type"`                  // 装备类型 例：刀枪弓戟
	Name                string                 `json:"name"`                  // 装备名称
	Desc                string                 `json:"desc"`                  // 装备描述
	Picture             string                 `json:"picture"`               // 图标
	EquipmentInfoExtend []*EquipmentInfoExtend `json:"equipment_info_extend"` // 装备扩展信息
}

type EquipmentInfoExtend struct {
	Quality      int32 `json:"quality"`       // 品质1-5
	SpeciesPrice int32 `json:"species_price"` // 金币价格
	DiamondPrice int32 `json:"diamond_price"` // 钻石价格
	//CaloricValue int32              `json:"caloric_value"` // 热值
	Effects  []*AttributeEffect `json:"effects"`   // 属性影响
	CoolTime int32              `json:"cool_time"` // 冷却时间
}

type GetsEquipmentInfoReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}

type GetsEquipmentInfoRes struct {
	Page           int32            `json:"page"`
	PerPage        int32            `json:"per_page"`
	Total          int32            `json:"total"`
	EquipmentInfos []*EquipmentInfo `json:"equipment_infos"`
}

type CreateEquipmentInfoReq struct {
	*EquipmentInfo
}

type CreateEquipmentInfoRes struct {
	*EquipmentInfo
}

type UpdateEquipmentInfoReq struct {
	*EquipmentInfo
}

type UpdateEquipmentInfoRes struct {
	*EquipmentInfo
}
