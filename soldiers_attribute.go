package model

//type SoldiersAttribute struct {
//	ArmTypeId         int32             `json:"arm_type_id"`        //兵种
//	Level             int32             `json:"level"`              //等级
//	SoldiersAttribute map[int32]float32 `json:"soldiers_attribute"` //兵种属性
//}
//
//type GetsSoldiersAttributeReq struct {
//	Page    int32 `form:"page"`
//	PerPage int32 `form:"per_page"`
//}
//
//type GetsSoldiersAttributeRes struct {
//	SoldiersAttribute []*SoldiersAttribute `json:"soldiers_attribute"`
//}
//
//type CreateSoldiersAttributeReq struct {
//	*SoldiersAttribute
//}
//
//type CreateSoldiersAttributeRes struct {
//	*SoldiersAttribute
//}
//
//type UpdateSoldiersAttributeReq struct {
//	*SoldiersAttribute
//}
//
//type UpdateSoldiersAttributeRes struct {
//	*SoldiersAttribute
//}

// 兵种表

type ArmyAttribute struct {
	Id              int32
	ArmyTypeId      int32             `json:"army_type_id"`      // 兵种类型
	Level           int32             `json:"level"`             // 等级（也就是t几兵）
	ConsumeExp      int32             `json:"consume_exp"`       // 消耗经验值
	ArmyAttribute   map[int32]float32 `json:"army_attribute"`    // 属性
	Restrain        bool              `json:"restrain"`          // 克制关系
	UpAttributeId   []int32           `json:"up_attribute_id"`   // 上一级兵种编号
	NextAttributeId []int32           `json:"next_attribute_id"` // 下一个兵种编号
	AttributeName   string            `json:"attribute_name"`    // 名称
	Picture         string            `json:"picture"`           // 图片地址
}

type GetsArmyAttributeReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}

type GetsArmyAttributeRes struct {
	ArmyAttribute []*ArmyAttribute `json:"army_attribute"`
}

type CreateArmyAttributeReq struct {
	*ArmyAttribute
}

type CreateArmyAttributeRes struct {
	*ArmyAttribute
}

type UpdateArmyAttributeReq struct {
	*ArmyAttribute
}

type UpdateArmyAttributeRes struct {
	*ArmyAttribute
}
