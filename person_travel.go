package model

type TravelFoodInfo struct {
	Id          int32   `json:"id"`           // 编号
	FoodId      int64   `json:"food_id"`      // 食物ID（物品ID）
	Name        string  `json:"name"`         // 名称
	FootQuality int32   `json:"foot_quality"` // 食物品质
	TravelTime  int32   `json:"travel_time"`  // 旅行时间
	Desc        string  `json:"desc"`         // 描述
	Weight      float32 `json:"weight"`       // 倍率
	RewardId    []int32 `json:"reward_id"`    // 奖励ID
}

type GetsTravelFoodInfoReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}
type GetsTravelFoodInfoRes struct {
	Page           int32             `json:"page"`
	PerPage        int32             `json:"per_page"`
	Total          int32             `json:"total"`
	TravelFoodInfo []*TravelFoodInfo `json:"travel_food_info"`
}

type CreateTravelFoodInfoReq struct {
	*TravelFoodInfo
}

type CreateTravelFoodInfoRes struct {
	*TravelFoodInfo
}

type UpdateTravelFoodInfoReq struct {
	*TravelFoodInfo
}

type UpdateTravelFoodInfoRes struct {
	*TravelFoodInfo
}

/////////////////////////////////////////////////////////////////////////////

type TravelTreasureInfo struct {
	Id           int32   `json:"id"`          // 编号
	TreasureID   int64   `json:"treasure_id"` // 宝物ID（物品ID）
	Name         string  `json:"name"`        // 宝物名称
	TreasureDesc string  `json:"desc"`        // 描述
	TravelDesc   string  `json:"travel_desc"` // 旅行描述
	RewardId     []int32 `json:"reward_id"`   // 奖励ID
}

type GetsTravelTreasureInfoReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}
type GetsTravelTreasureInfoRes struct {
	Page               int32                 `json:"page"`
	PerPage            int32                 `json:"per_page"`
	Total              int32                 `json:"total"`
	TravelTreasureInfo []*TravelTreasureInfo `json:"travel_treasure_info"`
}

type CreateTravelTreasureInfoReq struct {
	*TravelTreasureInfo
}

type CreateTravelTreasureInfoRes struct {
	*TravelTreasureInfo
}

type UpdateTravelTreasureInfoReq struct {
	*TravelTreasureInfo
}

type UpdateTravelTreasureInfoRes struct {
	*TravelTreasureInfo
}

type TravelAdventureInfo struct {
	AdventureId   int32   `json:"adventure_id"`   // 事件编号
	ArticleId     int32   `json:"article_id"`     // 触发事件的ID（物品ID）
	Survival      bool    `json:"survival"`       // 族人存活(0存活,1死亡)
	AdventureName string  `json:"adventure_name"` // 事件名称
	Desc          string  `json:"desc"`           // 描述
	RewardId      []int32 `json:"reward_id"`      // 奖励ID
}

type GetsTravelAdventureInfoReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}
type GetsTravelAdventureInfoRes struct {
	Page                int32                  `json:"page"`
	PerPage             int32                  `json:"per_page"`
	Total               int32                  `json:"total"`
	TravelAdventureInfo []*TravelAdventureInfo `json:"travel_adventure_info"`
}

type CreateTravelAdventureInfoReq struct {
	*TravelAdventureInfo
}

type CreateTravelAdventureInfoRes struct {
	*TravelAdventureInfo
}

type UpdateTravelAdventureInfoReq struct {
	*TravelAdventureInfo
}

type UpdateTravelAdventureInfoRes struct {
	*TravelAdventureInfo
}
