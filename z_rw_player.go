package model

// Player 初始化的角色
type Player struct {
	Id       int64  `json:"id" validate:"required"`
	Name     string `json:"name" validate:"required"`       //玩家名称
	AvatarId int32  `json:"avatar_id"  validate:"required"` //玩家头像
	//Status   int32  `json:"status"`                         //玩家状态
	//Combat    int32  `json:"fight"`
	//Trade     int32  `json:"trade"`
	//ChapterId int32  `json:"chapter_id"`
	//BattleId  int32  `json:"battle_id"`
	//HasIm     bool   `json:"has_im"`
}

type GetsPlayerReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}

type GetsPlayerRes struct {
	Page    int32     `json:"page"`
	PerPage int32     `json:"per_page"`
	Total   int32     `json:"total"`
	Players []*Player `json:"players"`
}

type CreatePlayerReq struct {
	*Player
}

type CreatePlayerRes struct {
	*Player
}

type UpdatePlayerReq struct {
	*Player
}

type UpdatePlayerRes struct {
	*Player
}
