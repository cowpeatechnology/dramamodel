package model

// Attribute 属性 例：力量，智力，洞察，敏捷，耐力，幸运，魅力，速度
type Attribute struct {
	AttributeId int32  `json:"attribute_id" validate:"required,gte=0"` // 属性编号
	Name        string `json:"name" validate:"required"`               // 名称
}

// AttributeEffect 属性影响【包装】
type AttributeEffect struct {
	AttributeId int32   `json:"attribute_id" validate:"required"` // 属性编号
	Value       float32 `json:"value" validate:"required"`        // 属性加成值
}

type GetsAttributeReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}

type GetsAttributeRes struct {
	Page       int32        `json:"page"`
	PerPage    int32        `json:"per_page"`
	Total      int32        `json:"total"`
	Attributes []*Attribute `json:"attributes"`
}

type CreateAttributeReq struct {
	*Attribute
}

type CreateAttributeRes struct {
	*Attribute
}

type UpdateAttributeReq struct {
	*Attribute
}

type UpdateAttributeRes struct {
	*Attribute
}
