package model

// ExtendForgeShop 锻造坊类扩展表
type ExtendForgeShop struct {
	// 家园建筑信息编号
	HouseInfoID int32 `json:"house_info_id"`
	// 装备减少时间
	PositionId int32 `json:"position_id"`
	// 装备减少时间
	ReduceTime string `json:"reduce_time"`
}
type GetsExtendForgeShopReq struct {
}

type GetsExtendForgeShopRes struct {
	ExtendForgeShops []*ExtendForgeShop `json:"extend_forge_shops"`
}

type CreateExtendForgeShopReq struct {
	*ExtendForgeShop
}

type CreateExtendForgeShopRes struct {
	*ExtendForgeShop
}

type UpdateExtendForgeShopReq struct {
	*ExtendForgeShop
}

type UpdateExtendForgeShopRes struct {
	*ExtendForgeShop
}

/////////////////////////////////////////////////////////////////////////////////////////////////

// ExtendResourceOutputs 产出资源扩展表
type ExtendResourceOutputs struct {
	// 家园建筑信息编号
	HouseInfoID int32 `json:"house_info_id"`
	// 产出资源的编号
	ResourceId int32 `json:"resource_id"`
	// 产出数量/每小时
	Outputs string `json:"outputs"`
	// 最大存储量
	MaxStorage string `json:"max_storage"`
}

type GetsExtendResourceOutputsReq struct {
}

type GetsExtendResourceOutputsRes struct {
	ExtendResourceOutputs []*ExtendResourceOutputs `json:"extend_resource_outputs"`
}

type CreateExtendResourceOutputsReq struct {
	*ExtendResourceOutputs
}

type CreateExtendResourceOutputsRes struct {
	*ExtendResourceOutputs
}

type UpdateExtendResourceOutputsReq struct {
	*ExtendResourceOutputs
}

type UpdateExtendResourceOutputsRes struct {
	*ExtendResourceOutputs
}

/////////////////////////////////////////////////////////////////////////////////////////////////

// ExtendSpecial 熔炼建筑拓展表
type ExtendSpecial struct {
	// 家园建筑信息编号
	HouseInfoID int32 `json:"house_info_id"`
	// 热值上限
	CoolTimeValue string `json:"cool_time_value"` // 升级减少冷却时间百分比
}

type GetsExtendSpecialReq struct {
}

type GetsExtendSpecialRes struct {
	ExtendSpecials []*ExtendSpecial `json:"extend_specials"`
}

type CreateExtendSpecialReq struct {
	*ExtendSpecial
}

type CreateExtendSpecialRes struct {
	*ExtendSpecial
}

type UpdateExtendSpecialReq struct {
	*ExtendSpecial
}

type UpdateExtendSpecialRes struct {
	*ExtendSpecial
}

/////////////////////////////////////////////////////////////////////////////////////////////////

// ExtendMainCity 主城
type ExtendMainCity struct {
	// 家园建筑信息编号
	HouseInfoID int32 `json:"house_info_id"`
	// 同时锻造最大数
	EquipMax string `json:"equip_max"`
	// 访客数（来购买东西的Npc）
	NumberVisitors string `json:"number_visitors"`
	// 出售友善度
	ObtainGoodOpinion string `json:"obtain_good_opinion"`
	// 好感度上限 （科技加主城等级）
	MaxGoodOpinion string `json:"max_good_opinion"`
}

type GetsExtendMainCityReq struct {
}

type GetsExtendMainCityRes struct {
	ExtendMainCities []*ExtendMainCity `json:"extend_main_cities"`
}

type CreateExtendMainCityReq struct {
	*ExtendMainCity
}

type CreateExtendMainCityRes struct {
	*ExtendMainCity
}

type UpdateExtendMainCityReq struct {
	*ExtendMainCity
}

type UpdateExtendMainCityRes struct {
	*ExtendMainCity
}
