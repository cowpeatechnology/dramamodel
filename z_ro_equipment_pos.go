package model

// EquipmentPos 装备位置
type EquipmentPos struct {
	EquipmentPosId int32  `json:"equipment_pos_id" validate:"required,gte=0"` // 装备位置编号
	Name           string `json:"name"  validate:"required"`                  // 名称
}

type GetsEquipmentPosReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}

type GetsEquipmentPosRes struct {
	Page           int32           `json:"page"`
	PerPage        int32           `json:"per_page"`
	Total          int32           `json:"total"`
	EquipmentPoses []*EquipmentPos `json:"equipment_poses"`
}

type CreateEquipmentPosReq struct {
	*EquipmentPos
}

type CreateEquipmentPosRes struct {
	*EquipmentPos
}

type UpdateEquipmentPosReq struct {
	*EquipmentPos
}

type UpdateEquipmentPosRes struct {
	*EquipmentPos
}
