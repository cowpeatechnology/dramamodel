package model

// MapPath 有效路径
type MapPath struct {
	MapPathId int32 `json:"map_path_id" validate:"required,gte=0"` // 路径编号
	LPosId    int32 `json:"l_pos_id" validate:"required"`          // 较小的 Pos 编号（避免不同组合）
	GPosId    int32 `json:"g_pos_id" validate:"required"`          // 较大的 Pos 编号（避免不同组合）
}

type GetsMapPathReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}

type GetsMapPathRes struct {
	Page     int32      `json:"page"`
	PerPage  int32      `json:"per_page"`
	Total    int32      `json:"total"`
	MapPaths []*MapPath `json:"map_paths"`
}

type CreateMapPathReq struct {
	*MapPath
}

type CreateMapPathRes struct {
	*MapPath
}

type UpdateMapPathReq struct {
	*MapPath
}

type UpdateMapPathRes struct {
	*MapPath
}

// MapPathWithPos 用于回传地图编辑器
type MapPathWithPos struct {
	MapPathID int32 `json:"map_path_id"`

	LPosID int32 `json:"l_pos_id"`
	LPosX  int32 `json:"l_pos_x"`
	LPosY  int32 `json:"l_pos_y"`

	GPosID int32 `json:"g_pos_id"`
	GPosX  int32 `json:"g_pos_x"`
	GPosY  int32 `json:"g_pos_y"`
}
