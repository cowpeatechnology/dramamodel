package model

// Recipe 配方
type Recipe struct {
	RecipeId              int32          `json:"recipe_id" validate:"required,gte=0"` // 配方编号
	RecipeItemId          int32          `json:"recipe_item_id"`                      // 配方关联道具编号
	RecipeEquipmentInfoId int32          `json:"recipe_equipment_info_id"`            // 配方关联装备信息编号
	RecipeHouseId         int32          `json:"recipe_house_id"`                     // 配方关联建筑编号
	HouseLevel            int32          `json:"house_level"`                         // 配方等级（对应建筑等级解锁配方）
	RecipeClass           int32          `json:"recipe_class"`                        // 配方阶级（目前仅供分类展示）
	ProductionTime        int64          `json:"production_time"`                     // 制作时间
	RecipeName            string         `json:"recipe_name"`                         // 配方名称
	RecipeDesc            string         `json:"recipe_desc"`                         // 描述
	Picture               string         `json:"picture"`                             // 图标
	Proficiencies         []*Proficiency `json:"proficiencies"`                       // 熟练度
	RecipeCosts           []*RecipeCosts `json:"recipe_costs" validate:"required"`    // 配方材料
}

type RecipeCosts struct {
	ItemId   int32 `json:"item_id" validate:"required"`
	ItemType int32 `json:"item_type"` // type 为1 为物品，typ 为2 为装备 （暂时只能是物品）
	Count    int32 `json:"count" validate:"min=1"`
}

type Proficiency struct {
	GradedPoint   []int32 `json:"graded_point"`   // 阶段(熟练度进阶点)
	AdditionType  int32   `json:"addition_type"`  // 加成类（消耗减少，售价增加，品质增加，属性增加。。。）
	Index         int32   `json:"index"`          // 加成的ID(消耗减少的物品ID)
	Value         []int32 `json:"value"`          // 加成值
	GradedQuality []int32 `json:"graded_quality"` // 阶段品质
}

type RecipePremise struct {
	RecipeId int32 `json:"recipe_id" validate:"required,gt=0"`
	Level    int32 `json:"level" validate:"required"`
}

type GetsRecipeReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}

type GetsRecipeRes struct {
	Page    int32     `json:"page"`
	PerPage int32     `json:"per_page"`
	Total   int32     `json:"total"`
	Recipes []*Recipe `json:"recipes"`
}

type CreateRecipeReq struct {
	*Recipe
}

type CreateRecipeRes struct {
	*Recipe
}

type UpdateRecipeReq struct {
	*Recipe
}

type UpdateRecipeRes struct {
	*Recipe
}
