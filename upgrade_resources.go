package model

type UpgradeResources struct {
	Id         int32        `gorm:"primary_key" json:"id"`
	ArmyType   int32        `json:"army_type"`   //兵种类型
	Level      int32        `json:"level"`       //升级方案
	GoodCounts []*GoodCount `json:"good_counts"` //升级所需物品材料
}

type GetsUpgradeResourcesReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}

type GetsUpgradeResourcesRes struct {
	Page             int32               `json:"page"`
	PerPage          int32               `json:"per_page"`
	Total            int32               `json:"total"`
	UpgradeResources []*UpgradeResources `json:"upgrade_resources"`
}

type CreateUpgradeResourcesReq struct {
	*UpgradeResources
}

type CreateUpgradeResourcesRes struct {
	*UpgradeResources
}

type UpdateUpgradeResourcesReq struct {
	*UpgradeResources
}

type UpdateUpgradeResourcesRes struct {
	*UpgradeResources
}
