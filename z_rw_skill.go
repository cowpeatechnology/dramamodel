package model

// Skill 技能
// 包含多个子技能的包装(SubSkillUsage)
// 例：破釜沉舟
type Skill struct {
	Id      int32   `json:"id" validate:"min=0"`
	Quality int32   `json:"quality" validate:"required"` //技能品质
	Type    int32   `json:"type" validate:"required"`    //技能类型
	Prob    float32 `json:"prob" validate:"required"`    //技能概率
	Name    string  `json:"name" validate:"required"`    //技能名称
	Desc    string  `json:"desc"`                        //技能描述

	Picture string `json:"picture"` // 技能图标

	ConditionCriterion int32   `json:"condition_criterion"` // 条件依据
	ConditionTarget    int32   `json:"condition_target"`    // 条件人
	ConditionValue     []int32 `json:"condition_value"`     // 判断值 // 根据上下文

	SubSkillUsageList []*SubSkillUsage `json:"sub_skill_usage_list"`
}

type CreateSkillReq struct {
	*Skill
}

type CreateSkillRes struct {
	*Skill
}

type UpdateSkillReq struct {
	*Skill
}

type UpdateSkillRes struct {
	*Skill
}

type GetsSkillReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
	Type    int32 `form:"type"`
}

type GetsSkillRes struct {
	Page    int32    `json:"page"`
	PerPage int32    `json:"per_page"`
	Type    int32    `json:"type"`
	Total   int32    `json:"total"`
	Skill   []*Skill `json:"skills"`
}

///////////////////////////////////////////////////////////////

// SubSkill 子技能
// 例：物理攻击
type SubSkill struct {
	Id      uint32 `json:"id"`            // 子技能编号
	Type    uint32 `json:"combat_effect"` // 技能效果
	Name    string `json:"name"`          // 子技能名称
	Formula string `json:"formula"`       // 结果值计算公式 `<src1> * 100 + 10`
}

type CreateSubSkillReq struct {
	*SubSkill
}

type CreateSubSkillRes struct {
	*SubSkill
}

type UpdateSubSkillReq struct {
	*SubSkill
}

type UpdateSubSkillRes struct {
	*SubSkill
}

type GetsSubSKillReq struct {
	SkillId int32 `form:"skill_id"`
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}

type GetsSubSKillRes struct {
	Page     int32       `json:"page"`
	PerPage  int32       `json:"per_page"`
	Total    int32       `json:"total"`
	SubSkill []*SubSkill `json:"sub_skill"`
}

// SubSkillUsage 子技能的包装
type SubSkillUsage struct {
	SubSkillId      uint32  `json:"sub_skill_id"`     // 子技能编号
	Loop            int32   `json:"loop"`             // 执行次数
	ExecutionRounds []int32 `json:"execution_rounds"` // 持续回合
	Prob            float32 `json:"prob"`             // 发动概率
	BaseValue       float32 `json:"base_value"`       // 技能值
	GrowthProb      float32 `json:"growth_prob"`      // 成长发动概率
	GrowthValue     float32 `json:"growth_value"`     // 成长技能值

	Sponsors   []uint32             `json:"sponsors"`   // 0 自己 | 1 己方主将 | 2 己方副将-1 | 3 己方副将-2 | 4 对方主将 | 5 对方副将-1 | 6 对方副将-2
	Receivers  []uint32             `json:"receivers"`  // 0 自己 | 1 己方主将 | 2 己方副将-1 | 3 己方副将-2 | 4 对方主将 | 5 对方副将-1 | 6 对方副将-2
	Status     []*StatusCondition   `json:"status"`     //子技能状态
	Conditions []*SubSkillCondition `json:"conditions"` //子技能条件判断

	Timing       int32   `json:"timing"`        // 条件时刻
	TimingTarget int32   `json:"timing_target"` //条件人
	TimingValue  []int32 `json:"timing_value"`  // 条件时刻-判断值 // 根据上下文

	SubSkillName string `json:"sub_skill_name"` //自定义子技能名称
}

type SubSkillCondition struct {
	SubConditionTarget    int32   `json:"sub_condition_target"`    // 条件人 // 0 自己 | 1 己方主将 | 2 己方副将-1 | 3 己方副将-2 | 4 对方主将 | 5 对方副将-1 | 6 对方副将-2
	SubConditionCriterion int32   `json:"sub_condition_criterion"` // 条件依据 // 0 关闭条件依据（对之前版本的技能适配） | 1回合数 | 2 兵力 | 3 当前状态（BUFF） | 4 自己身份（判断值：0主将、1副将）
	SubConditionValue     []int32 `json:"sub_condition_value"`     // 判断值 // 根据上下文

}

type StatusCondition struct {
	StatusID      uint32 `json:"status_id"`      //状态编号
	ExecCount     int32  `json:"exec_count"`     //执行次数
	DurationRound int32  `json:"duration_round"` //状态持续回合
}
