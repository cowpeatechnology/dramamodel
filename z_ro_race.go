package model

// Race 种族
type Race struct {
	RaceId     int32   `json:"race_id" validate:"required,gte=0"` // 种族编号
	DrawProb   int32   `json:"draw_prob" validate:"required"`     // 抽取概率
	Name       string  `json:"name" validate:"required"`          // 种族名
	SpecialMax []int32 `json:"special_max" validate:"required"`   // 属性上限
}

type GetsRaceReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}

type GetsRaceRes struct {
	Page    int32   `json:"page"`
	PerPage int32   `json:"per_page"`
	Total   int32   `json:"total"`
	Races   []*Race `json:"races"`
}

type CreateRaceReq struct {
	*Race
}

type CreateRaceRes struct {
	*Race
}

type UpdateRaceReq struct {
	*Race
}

type UpdateRaceRes struct {
	*Race
}
