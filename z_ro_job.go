package model

// Job 职能
type Job struct {
	JobId int32  `json:"job_id" validate:"required,gte=0"` // 职能编号
	Name  string `json:"name"  validate:"required"`        // 名称
}

type GetsJobReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}

type GetsJobRes struct {
	Page    int32  `json:"page"`
	PerPage int32  `json:"per_page"`
	Total   int32  `json:"total"`
	Jobs    []*Job `json:"jobs"`
}

type CreateJobReq struct {
	*Job
}

type CreateJobRes struct {
	*Job
}

type UpdateJobReq struct {
	*Job
}

type UpdateJobRes struct {
	*Job
}
