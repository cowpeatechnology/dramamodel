package model

type GoodSubType struct {
	GoodSubTypeId int32  `json:"good_sub_type_id" validate:"required,gte=0"` // 道具子类型编号
	GoodTypeId    int32  `json:"good_type_id" validate:"required,gte=0"`     // 道具类型编号
	Name          string `json:"name"  validate:"required"`                  // 类型名称
}

type GetsGoodSubTypeReq struct {
	Page    int32 `form:"page"`
	PerPage int32 `form:"per_page"`
}

type GetsGoodSubTypeRes struct {
	Page         int32          `json:"page"`
	PerPage      int32          `json:"per_page"`
	Total        int32          `json:"total"`
	GoodSubTypes []*GoodSubType `json:"good_sub_types"`
}

type CreateGoodSubTypeReq struct {
	*GoodSubType
}

type CreateGoodSubTypeRes struct {
	*GoodSubType
}

type UpdateGoodSubTypeReq struct {
	*GoodSubType
}

type UpdateGoodSubTypeRes struct {
	*GoodSubType
}
